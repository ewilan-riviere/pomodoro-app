import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pomodoro/controller/countdown_controller.dart';
import 'package:pomodoro/shared/form/settings_form.dart';
import 'package:pomodoro/shared/widget/app_layout.dart';

class HomePage extends StatelessWidget {
  HomePage({super.key});

  final ct = Get.put(CountdownController());

  @override
  Widget build(BuildContext context) {
    return AppLayout(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            duration(),
            timer(),
            const SettingsForm(),
          ],
        ),
      ),
    );
  }

  Widget duration() {
    return Obx(
      () => Text(
        "Duration ${ct.base.value.toString()}",
      ),
    );
  }

  Widget timer() {
    return Obx(
      () => Text(
        ct.ct.value,
        style: const TextStyle(
          fontWeight: FontWeight.w500,
          color: Colors.white,
          fontFamily: 'Mono',
          fontSize: 20,
          fontFeatures: [
            FontFeature.tabularFigures(),
          ],
        ),
      ),
    );
  }
}
