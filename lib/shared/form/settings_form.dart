import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pomodoro/controller/countdown_controller.dart';
import 'package:pomodoro/services/notification_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsForm extends StatefulWidget {
  const SettingsForm({Key? key}) : super(key: key);

  @override
  State<SettingsForm> createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {
  final ct = Get.put(CountdownController());
  final _formKey = GlobalKey<FormState>();
  String _duration = '3000';

  TextEditingController durationController = TextEditingController();
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();
    _prefs.then((SharedPreferences prefs) {
      var local = prefs.getString('duration') ?? _duration;
      durationController.text = local;
    });
  }

  void _submit() async {
    if (_formKey.currentState!.validate()) {
      await ct.updateSettings(_duration);

      NotificationService.snackbar('Duration saved');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 15, bottom: 8.0),
            child: TextFormField(
              controller: durationController,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                // FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                FilteringTextInputFormatter.digitsOnly
              ],
              decoration: const InputDecoration(
                hintText: "Duration",
                labelText: 'Duration',
                // icon: Icon(CupertinoIcons.clock),
                border: OutlineInputBorder(),
              ),
              validator: (text) {
                if (text == null || text.isEmpty) {
                  return 'Can\'t be empty';
                }

                return null;
              },
              // update the state variable when the text changes
              onChanged: (text) => setState(() => _duration = text),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Builder(
              builder: (context) {
                return CupertinoButton(
                  onPressed: _duration.isNotEmpty ? _submit : null,
                  child: const Text(
                    'Save',
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
