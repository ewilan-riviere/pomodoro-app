import 'package:flutter/material.dart';

class AppDialog extends StatelessWidget {
  const AppDialog({super.key, this.title, this.children});

  final String? title;
  final List<Widget>? children;

  @override
  Widget build(BuildContext context) {
    var scrollController = ScrollController();
    List<Widget> list = [];
    if (children != null) {
      list.insertAll(0, children!);
    }

    return Dialog(
      child: SizedBox(
        // height: 300,
        child: SingleChildScrollView(
          controller: scrollController,
          child: Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                title != null
                    ? Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                        child: Text(
                          title!,
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      )
                    : Container(),
                ...list,
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text("Close"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
