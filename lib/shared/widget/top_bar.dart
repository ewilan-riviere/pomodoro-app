import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pomodoro/controller/countdown_controller.dart';
import 'package:window_manager/window_manager.dart';

class TopBar extends StatefulWidget {
  const TopBar({Key? key}) : super(key: key);
  static const double _topBarHeight = 50;
  final double topBarHeight = _topBarHeight;

  @override
  State<TopBar> createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> {
  @override
  Widget build(BuildContext context) {
    final ct = Get.put(CountdownController());

    return AppBar(
      toolbarHeight: widget.topBarHeight,
      title: _buildLeadingWidget(context),
      automaticallyImplyLeading: false,
      elevation: 0,
      backgroundColor: Colors.grey.shade900,
      actions: [
        Obx(
          () => ct.isPlaying.value
              ? iconAction(
                  context: context,
                  icon: CupertinoIcons.pause,
                  onPressed: () => ct.pause(),
                )
              : iconAction(
                  context: context,
                  icon: CupertinoIcons.play_arrow,
                  onPressed: () => ct.play(),
                ),
        ),
        iconAction(
          context: context,
          icon: CupertinoIcons.arrow_clockwise,
          onPressed: () {
            ct.reset();
          },
        ),
        iconAction(
          context: context,
          icon: CupertinoIcons.eye_slash,
          onPressed: () {
            windowManager.hide();
          },
        ),
      ],
    );
  }

  Widget iconAction({
    required BuildContext context,
    IconData icon = CupertinoIcons.add,
    Function? onPressed,
  }) {
    return IconButton(
      icon: Icon(
        icon,
        size: 20,
        color: Colors.grey,
      ),
      splashRadius: 20,
      onPressed: () {
        if (onPressed != null) {
          onPressed();
        }
      },
    );
  }

  Widget _buildLeadingWidget(BuildContext context) {
    final ScaffoldState scaffold = Scaffold.of(context);
    final ModalRoute<Object?>? parentRoute = ModalRoute.of(context);

    // final bool isHome = AppRouter.currentRoute(context) == AppRoute.home;
    bool canPop = ModalRoute.of(context)?.canPop ?? false;
    // if (isHome) {
    //   canPop = false;
    // }
    final bool hasDrawer = scaffold.hasDrawer;
    final bool useCloseButton =
        parentRoute is PageRoute<dynamic> && parentRoute.fullscreenDialog;

    Widget leading = const SizedBox();
    if (hasDrawer) {
      leading = IconButton(
        icon: const Icon(Icons.menu_rounded),
        onPressed: Scaffold.of(context).openDrawer,
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      );
    } else {
      if (canPop) {
        if (useCloseButton) {
          leading = IconButton(
            color: Theme.of(context).colorScheme.onBackground,
            icon: const Icon(Icons.close_rounded),
            onPressed: () => Navigator.of(context).maybePop(),
          );
        } else {
          leading = IconButton(
            iconSize: 25,
            splashRadius: 20,
            icon: const Icon(
              CupertinoIcons.back,
              color: Colors.grey,
            ),
            onPressed: Navigator.of(context).pop,
          );
        }
      }
    }

    return Padding(padding: const EdgeInsets.only(left: 60), child: leading);
  }
}
