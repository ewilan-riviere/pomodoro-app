import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pomodoro/controller/countdown_controller.dart';
import 'package:pomodoro/services/tray_service.dart';
import 'package:pomodoro/shared/widget/top_bar.dart';
import 'package:window_manager/window_manager.dart';

var tray = TrayService.init();

class AppLayout extends StatelessWidget with WindowListener {
  const AppLayout({super.key, required this.child});

  final Widget child;

  Future<void> _init() async {
    await windowManager.setPreventClose(true);
    tray = await TrayService.make();
  }

  @override
  Widget build(BuildContext context) {
    var controller = ScrollController();
    final ct = Get.put(CountdownController());
    _init();

    ct.isPlaying.listen((isPlaying) async {
      await tray.watchIcon(isPlaying);
    });

    return Container(
      constraints: const BoxConstraints.expand(),
      child: Scaffold(
        appBar: const PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: TopBar(),
        ),
        body: SingleChildScrollView(
          controller: controller,
          child: Container(
            padding: const EdgeInsets.all(30),
            child: child,
          ),
        ),
      ),
    );
  }

  @override
  void onWindowClose() async {
    await windowManager.destroy();
  }
}
