import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pomodoro/shared/setup.dart';
import 'package:pomodoro/views/home_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Setup.make();

  runApp(const PomodoroApp());
}

class PomodoroApp extends StatelessWidget {
  const PomodoroApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Pomodoro',
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
