import 'dart:io';

import 'package:get/get.dart';
import 'package:pomodoro/controller/countdown_controller.dart';
import 'package:system_tray/system_tray.dart';
import 'package:window_manager/window_manager.dart';

class TrayService {
  TrayService({
    required this.appWindow,
    required this.systemTray,
    this.icon = 'assets/icons/system-tray.png',
    this.title = 'Pomodoro',
    required this.menu,
  });

  AppWindow appWindow;
  SystemTray systemTray;
  String icon;
  String title;
  Menu menu;

  static TrayService init() {
    return TrayService(
      appWindow: AppWindow(),
      systemTray: SystemTray(),
      menu: Menu(),
    );
  }

  static Future<TrayService> make() async {
    var self = TrayService(
      appWindow: AppWindow(),
      systemTray: SystemTray(),
      menu: Menu(),
    );
    self.icon = self._setIcon();
    await self._init();

    return self;
  }

  Future<void> watchIcon(bool isPlaying) async {
    if (isPlaying) {
      icon = 'assets/icons/system-tray-color.png';
    } else {
      icon = 'assets/icons/system-tray.png';
    }
    await systemTray.setImage(icon);
    // print(icon);
    // await _init();
  }

  Future<void> _setMenu() async {
    final ct = Get.put(CountdownController());
    await menu.buildFrom([
      MenuItemLabel(
        label: 'Play/Pause',
        onClicked: (menuItem) async {
          if (ct.isPlaying.value) {
            ct.pause();
          } else {
            ct.play();
          }
        },
      ),
      MenuItemLabel(
        label: 'Reset',
        onClicked: (menuItem) async {
          ct.reset();
        },
      ),
      MenuSeparator(),
      MenuItemLabel(
        label: 'Toggle window',
        onClicked: (menuItem) async {
          var isFocused = await windowManager.isFocused();
          var isVisible = await windowManager.isVisible();
          if (isFocused && isVisible) {
            windowManager.hide();
          } else if (!isFocused) {
            windowManager.show();
            windowManager.focus();
          } else {
            windowManager.show();
          }
        },
      ),
      MenuItemLabel(
        label: 'Exit',
        onClicked: (menuItem) async {
          await windowManager.destroy();
        },
      ),
    ]);
  }

  Future<void> _init() async {
    await systemTray.initSystemTray(
      iconPath: icon,
    );

    await _setMenu();
    await systemTray.setContextMenu(menu);

    systemTray.registerSystemTrayEventHandler((eventName) {
      if (eventName == kSystemTrayEventClick) {
        Platform.isWindows ? appWindow.show() : systemTray.popUpContextMenu();
      } else if (eventName == kSystemTrayEventRightClick) {
        Platform.isWindows ? systemTray.popUpContextMenu() : appWindow.show();
      }
    });
  }

  String _setIcon() {
    var icon = Platform.isWindows
        ? 'assets/icons/system-tray.png'
        : 'assets/icons/system-tray.png';

    return icon;
  }
}
