import 'package:just_audio/just_audio.dart';

class AudioService {
  AudioService({
    required this.player,
    this.fromUri = false,
    this.source,
  });

  AudioPlayer player;
  bool fromUri = false;
  String? source;

  static Future<AudioService> make({
    required String source,
    bool fromUri = false,
  }) async {
    var service =
        AudioService(player: AudioPlayer(), fromUri: fromUri, source: source);

    service.player.playbackEventStream.listen((event) {},
        onError: (Object e, StackTrace stackTrace) {
      // print('A stream error occurred: $e');
    });

    return service;
  }

  Future<void> play() async {
    try {
      var src = fromUri ? _fromUri() : _fromAsset();
      if (src != null) {
        await player.setAudioSource(src);
      }
    } catch (e) {
      // print("Error loading audio source: $e");
    }

    await player.play();
  }

  AudioSource? _fromUri() {
    if (source == null) {
      return null;
    }

    return AudioSource.uri(Uri.parse(source!));
  }

  AudioSource? _fromAsset() {
    if (source == null) {
      return null;
    }

    return AudioSource.asset(source!);
  }
}
