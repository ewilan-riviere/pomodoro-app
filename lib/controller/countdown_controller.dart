import 'dart:async';

import 'package:get/get.dart';
import 'package:pomodoro/services/audio_service.dart';
import 'package:pomodoro/services/notification_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CountdownController extends GetxController {
  var base = 3000.obs;
  var isPlaying = false.obs;
  var isStopped = false.obs;
  Timer? timer;
  var duration = const Duration(seconds: 1).obs;

  var ct = '00:00:00'.obs;

  updateSettings(String time) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('duration', time);
    var local = prefs.getString('duration') ?? '3000';
    base.value = int.parse(local);

    duration.value = Duration(seconds: base.value);
  }

  void setCountdown() {
    String strDigits(int n) => n.toString().padLeft(2, '0');
    final days = strDigits(duration.value.inDays);
    final hours = strDigits(duration.value.inHours.remainder(24));
    final minutes = strDigits(duration.value.inMinutes.remainder(60));
    final seconds = strDigits(duration.value.inSeconds.remainder(60));

    ct.value = '$hours:$minutes:$seconds';
  }

  void play() {
    isPlaying.value = true;
    duration.value = Duration(seconds: base.value);

    timer = Timer.periodic(
      const Duration(seconds: 1),
      (_) async {
        const reduceSecondsBy = 1;
        final seconds = duration.value.inSeconds - reduceSecondsBy;
        if (seconds < 0) {
          timer?.cancel();
          reset();
          restart();
          await playSound();
          NotificationService.notification('End of timer');
        } else {
          duration.value = Duration(seconds: seconds);
          setCountdown();
        }
      },
    );
  }

  void pause() {
    isPlaying.value = false;
    timer?.cancel();
  }

  void restart() {
    isStopped.value = false;
    isPlaying.value = true;
    timer?.cancel();
    duration.value = Duration(seconds: base.value);
    setCountdown();
    play();
  }

  void reset() {
    isStopped.value = !isStopped.value;
    isPlaying.value = false;
    timer?.cancel();
    duration.value = Duration(seconds: base.value);
    setCountdown();
  }

  Future<void> playSound() async {
    var audio = await AudioService.make(source: 'assets/music/cocolint.mp3');
    await audio.play();
  }
}
