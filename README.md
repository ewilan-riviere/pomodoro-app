# Pomodoro

Pomodoro desktop app built with Flutter.

## Setup

```bash
flutter pub get
```

## Run

```bash
flutter run
```

## Build

```bash
flutter build macos
```
